## LASTZ - pairwise DNA sequence aligner (v. 1.04.00-hotfix)

LASTZ is a program for aligning DNA sequences, a pairwise aligner, by Bob Harris. Its official git repository can be found [here](https://github.com/lastz/lastz). LASTZ is a great software but has a bug that, depending on the parameters choice, outputs inconsistent data. The authors of LASTZ are aware and have already fixed this bug, but there is no public release containing the fix yet. Since the public release of the [ANGORA workflow](https://gitlab.ub.uni-bielefeld.de/gi/angora) (ANncestral reconstruction by local GenOme Rearrangement Analysis) requires a corrected version of LASTZ, this repository provides a custom LASTZ hotfix version. Probably it will be removed as soon we have a new official LASTZ public release.

This hotfix is based on LASTZ 1.04.00, from Mar 13, 2017. The only changes made to this version are (i) a fix in `src/lastz.c` to solve the inconsistent output problem (for output files in format <tt>.lav</tt>), and (ii) a few compiling options added to the `src/Makefile` file to prevent recent compilers to generate compilation errors.

More specifically, in `src/lastz.c`:

```diff
--- src/lastz.c-orig    2017-03-12 22:36:58.000000000 +0100
+++ src/lastz.c 2019-07-10 17:21:33.618233111 +0200
@@ -3448,6 +3448,7 @@
        free_if_valid ("finish_one_strand (targetRev)", targetRev);
    if ((_queryRev  == NULL) && (queryRev  != NULL))  // (it was allocated locally)
        free_if_valid ("finish_one_strand (queryRev)", queryRev);
+   init_output_for_strand (); // make sure this it called before the next strand
    }   
 
 //----------
```

In `src/Makefile`:

```diff
--- src/Makefile-orig	2017-03-12 22:36:58.000000000 +0100
+++ src/Makefile	2019-07-10 17:21:33.618233111 +0200
@@ -54,7 +54,7 @@
 #
 #---------
 
-definedForAll = -Wall -Wextra -Werror -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE
+definedForAll = -Wall -Wextra -Werror -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -Wno-implicit-fallthrough -Wno-misleading-indentation
 flagsFor32    = -Dmax_sequence_index=32 -Dmax_malloc_index=40 -Ddiag_hash_size=4194304
 
 allowBackToBackGaps ?= 0                       # by default allowBackToBackGaps
```